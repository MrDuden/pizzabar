﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace PizzaBar
{
    public class Customer : MonoBehaviour
    {
        [SerializeField]
        private Transform _platePoint;
        private CustomerConfig _config;
        private bool _eating = false;

        public void Init(CustomerConfig config)
        {
            _config = config;
        }

        private void Eat(FoodItem foodItem)
        {
            StartCoroutine(EatAsync(foodItem));
        }

        private IEnumerator EatAsync(FoodItem foodItem)
        {
            _eating = true;

            while (foodItem.PiecesCount > 0)
            {
                var piece = foodItem.TakePiece();
                piece.Consume();

                yield return new WaitForSeconds(_config.EatSpeed);
            }

            foodItem.Remove();
            _eating = false;
        }


        void OnTriggerEnter2D(Collider2D collider)
        {
            if (_eating) { return; }

            var conveyorElement = collider.GetComponent<ConveyorElement>();
            conveyorElement.Take();
      
            var foodItem = collider.GetComponent<FoodItem>();
            foodItem.transform.DOMove(_platePoint.position, StaticValues.FoodTakeSpeed).OnComplete(() => {Eat(foodItem); });
        }
    }
}