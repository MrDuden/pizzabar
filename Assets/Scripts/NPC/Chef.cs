﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace PizzaBar
{
    public class Chef : MonoBehaviour
    {
        public bool IsBusy { get { return _isBusy; } }

        private ChefConfig _context;
        private bool _isBusy = false;
        private Coroutine _makePizzaCoroutine;
        private CookingTable _table;

        public void Init(ChefConfig context, CookingTable table)
        {
            _context = context;
            _table = table;
        }

        public void StopCooking()
        {
            _isBusy = false;
            if(_makePizzaCoroutine != null)
            {
                StopCoroutine(_makePizzaCoroutine);
            }
        }

        public void StartCooking()
        { 
            if (_isBusy) { return; }

            _isBusy = true;
            _makePizzaCoroutine = StartCoroutine(MakePizzaAsync());
        }

        private IEnumerator MakePizzaAsync()
        {
            yield return new WaitForSeconds(_context.ServeSpeed);

            ServePizza();

            StartCoroutine(MakePizzaAsync());
        }

        private void ServePizza()
        {
            Pizza pizza = _context.Pizza.GetPizza();
            pizza.transform.position = transform.position;
            pizza.gameObject.SetActive(true);


            var pizzaScale = pizza.transform.localScale;
            pizza.transform.localScale = Vector3.zero;

            pizza.transform.DOMove(_table.ServePoint.position, StaticValues.FoodServeSpeed).OnComplete(() => { _table.BerSection.PutElement(pizza.GetComponent<ConveyorElement>()); });
            pizza.transform.DOScale(pizzaScale, StaticValues.FoodServeSpeed);
        }
    }
}
