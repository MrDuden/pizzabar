﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class ConsumeProductListener : MonoBehaviour
    {
        [SerializeField]
        private PlayerData _playerData;

        public void StartListen()
        {
            ConsumableProduct.OnConsume += OnProductConsume;
        }

        public void OnProductConsume(ConsumableProduct product)
        {
            _playerData.AddCoins(product.Price);
        }
    }
}