﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class PlayerCoinsObserver : MonoBehaviour
    {
        public UI UI;
        public PlayerData PlayerData;

        public void StartListen()
        {
            PlayerData.OnCoinsAdded += OnCoinsAdded;
        }

        private void OnCoinsAdded(int coins)
        {
            UI.CoinsPanel.SetCoins(PlayerData.Coins);
        }
    }
}
