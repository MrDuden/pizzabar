﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public static class StaticValues
    {
        public static float FoodServeSpeed = 0.5f;
        public static float FoodTakeSpeed = 0.5f;
    }
}