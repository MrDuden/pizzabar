﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class StraightSection : ConveyorSection
    {
        [SerializeField]
        private Transform _startPoint;
        [SerializeField]
        private Transform _destinationPoint;

        public override void FillWithBackgroundElements(ConveyorElement backgroundElementPrefab, float step)
        {
            var heading = _destinationPoint.position - _startPoint.position;
            var distance = heading.magnitude;
            var direction = heading / distance;
            var elementsCount = distance / step;

            for (int i = 0; i < elementsCount; i++)
            {
                ConveyorElement element = Instantiate(backgroundElementPrefab, _startPoint.position + (direction * step * i),Quaternion.identity);
                element.transform.up = -direction;

                PutElement(element);
            }
        }

        private void Update()
        {
            if (!_isOn) { return; }

            List<ConveyorElement> elements = new List<ConveyorElement>(_elements);

            foreach (ConveyorElement element in elements)
            {
                MoveElement(element);
            }
        }

        private void MoveElement(ConveyorElement element)
        {
            var distance = Vector3.Distance(element.transform.position, _destinationPoint.transform.position);

            if (distance < _speed * Time.deltaTime)
            {
                element.transform.position = _destinationPoint.transform.position;
                OnDestinationReached(element);
                return;
            }

            element.transform.position = Vector3.MoveTowards(element.transform.position, _destinationPoint.transform.position, _speed * Time.deltaTime);
            distance = Vector3.Distance(element.transform.position, _destinationPoint.transform.position);
        }
    }
}
