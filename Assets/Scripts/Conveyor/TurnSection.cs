﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class TurnSection : ConveyorSection
    {
        [SerializeField]
        private float _radius;
        [SerializeField]
        private Transform _startPoint;
        [SerializeField]
        private float _turnAngle;
        [SerializeField]
        private bool _mirrored;


        private void Update()
        {
            if (!_isOn) { return; }

            List<ConveyorElement> elements = new List<ConveyorElement>(_elements);

            foreach (ConveyorElement element in elements)
            {
                MoveElement(element);
            }
        }

        private void MoveElement(ConveyorElement element)
        {
            var targetStartRotation = element.transform.eulerAngles.z;
            var headingElement = element.transform.position - transform.position;
            var directionToElement = headingElement / _radius;

            var headingStartPoint = _startPoint.position - transform.position;
            var directionToStartPoint = headingStartPoint / _radius;

            var elementAngle = Vector3.Angle(directionToStartPoint, directionToElement);
            var stepTurnAngle = _turnAngle - elementAngle;

            if (stepTurnAngle > _speed * Time.deltaTime)
            {
                element.transform.RotateAround(transform.position, Vector3.forward, _speed * Time.deltaTime);
            }
            else
            {
                element.transform.eulerAngles = new Vector3(element.transform.eulerAngles.x, element.transform.eulerAngles.y, targetStartRotation + stepTurnAngle);
                OnDestinationReached(element);
            }
        }

        public override void SetSpeed(float speed)
        {
            this._speed = 180 * speed / (Mathf.PI * _radius);
        }

        public override void FillWithBackgroundElements(ConveyorElement backgroundElementPrefab, float step)
        {
            var distance = (Mathf.PI * _radius * _turnAngle) / 180;
            var elementsCount = distance / step;

            for (int i = 0; i < elementsCount; i++)
            {
                var angle = -i * Mathf.PI / elementsCount;
                Vector3 instantiatePoint;

                if (_mirrored)
                {
                    instantiatePoint = transform.position + (new Vector3(Mathf.Cos(angle), -Mathf.Sin(angle)) * _radius);
                }
                else
                {
                    instantiatePoint = transform.position + (new Vector3(-Mathf.Cos(angle), Mathf.Sin(angle)) * _radius);
                }

                ConveyorElement element = Instantiate(backgroundElementPrefab, instantiatePoint, Quaternion.identity);

                var heading = transform.position - element.transform.position;
                var direction = heading / _radius;

                element.transform.right = direction;
                if (_mirrored && i == 0)
                {
                    element.transform.up *= -1;
                }

                PutElement(element);
            }
        }
    }
}
