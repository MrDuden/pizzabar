﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class ConveyorElement : MonoBehaviour
    {
        private ConveyorSection _conveyorSection;

        public void Take()
        {
            _conveyorSection.RemoveElement(this);
        }

        public void Put(ConveyorSection conveyorSection)
        {
            _conveyorSection = conveyorSection;
        }
    }
}