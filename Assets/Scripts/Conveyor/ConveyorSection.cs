﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public abstract class ConveyorSection : MonoBehaviour
    {
        [SerializeField]
        protected ConveyorSection _nextSection = null;
        protected float _speed;
        protected List<ConveyorElement> _elements = new List<ConveyorElement>();
        protected bool _isOn = false;
        protected event Action<ConveyorElement> OnElementAdded;
        protected event Action OnElementRemoved;

        public void PutElement(ConveyorElement element)
        {
            _elements.Add(element);
            element.Put(this);

            OnElementAdded?.Invoke(element);
        }

        public void RemoveElement(ConveyorElement element)
        {
            _elements.Remove(element);
            OnElementRemoved?.Invoke();
        }

        public abstract void FillWithBackgroundElements(ConveyorElement backgroundElementPrefab, float step);

        public virtual void SetSpeed(float speed)
        {
            this._speed = speed;
        }

        public void TurnOn()
        {
            _isOn = true;
        }

        public void TurnOff()
        {
            _isOn = false;
        }

        protected void OnDestinationReached(ConveyorElement element)
        {
            RemoveElement(element);

            if (_nextSection != null)
            {
                _nextSection.PutElement(element);
            }
        }
    }
}