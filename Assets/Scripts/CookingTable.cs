﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class CookingTable : MonoBehaviour
    {
        public Transform ServePoint { get { return _servePoint; } }
        public ConveyorSection BerSection { get { return _barSection; } }
        public Chef Chef { get { return _chef; } }
        public bool IsTaken { get { return _chef == null ? true : false; } }

        [SerializeField]
        private Transform _servePoint;
        [SerializeField]
        private ConveyorSection _barSection;
        private Chef _chef;

        public void Take(Chef chef)
        {
            this._chef = chef;
        }

        public void Free()
        {
            _chef = null;
        }
    }
}