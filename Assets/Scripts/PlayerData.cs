﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class PlayerData : MonoBehaviour
    {
        public event Action<int> OnCoinsAdded;
        public event Action<int> OnCoinsRemoved;

        public int Coins { get { return _coins; } }
        private int _coins = 0;

        public void AddCoins(int coins)
        {
            _coins += coins;

            OnCoinsAdded?.Invoke(_coins);
        }

        public void RemoveCoins(int coins)
        {
            _coins -= coins;
            OnCoinsRemoved?.Invoke(_coins);
        }
    }
}