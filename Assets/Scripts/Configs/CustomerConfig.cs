﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    [CreateAssetMenu(fileName = "Customer Config", menuName = "Customer Config", order = 1)]
    public class CustomerConfig : ScriptableObject
    {
        public float EatSpeed;
    }
}