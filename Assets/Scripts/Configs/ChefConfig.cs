﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    [CreateAssetMenu(fileName = "Chef Config", menuName = "Chef Config", order = 1)]
    public class ChefConfig : ScriptableObject
    {
        public string Name;
        public float ServeSpeed;
        public PizzaConfig Pizza;
    }
}