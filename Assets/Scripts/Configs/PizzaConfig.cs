﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    [CreateAssetMenu(fileName = "Pizza Config", menuName = "Pizza Config", order = 1)]
    public class PizzaConfig : ScriptableObject
    {
        public int Price;
        public Sprite SliceSprite;
        public Pizza Prefab;

        public Pizza GetPizza()
        {
            Pizza pizza = Instantiate(Prefab);
            pizza.Init(SliceSprite, Price);

            return pizza;
        }
    }
}