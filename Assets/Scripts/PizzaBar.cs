﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class PizzaBar : MonoBehaviour
    {
        public int Speed;
        public int BackgroundElementsStep;

        [SerializeField]
        private ConveyorElement _prefab;
        [SerializeField]
        private List<ConveyorSection> sections;

        public void Init()
        {
            SetSpeed(Speed);

            foreach (ConveyorSection section in sections)
            {
                section.FillWithBackgroundElements(_prefab, BackgroundElementsStep);
            }
        }

        public void StartBar()
        {
            foreach (ConveyorSection section in sections)
            {
                section.TurnOn();
            }
        }

        public void StopBar()
        {
            foreach (ConveyorSection section in sections)
            {
                section.TurnOff();
            }
        }

        private void SetSpeed(int speed)
        {
            foreach (ConveyorSection section in sections)
            {
                section.SetSpeed(speed);
                section.FillWithBackgroundElements(_prefab, BackgroundElementsStep);
            }
        }
    }
}