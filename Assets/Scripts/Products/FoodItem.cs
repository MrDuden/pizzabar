﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public abstract class FoodItem : MonoBehaviour
    {
        public abstract int PiecesCount { get; }

        public abstract ConsumableProduct TakePiece();
        public abstract void Remove();
    }
}