﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public abstract class ConsumableProduct : MonoBehaviour
    {
        public static event Action<ConsumableProduct> OnConsume;

        public abstract int Price { get; }

        public abstract void Consume();

        public void CallConsumeEvent()
        {
            OnConsume?.Invoke(this);
        }
    }
}