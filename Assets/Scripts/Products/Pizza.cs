﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class Pizza : FoodItem
    {
        public override int PiecesCount { get { return _slices.Count; } }

        [SerializeField]
        private List<PizzaSlice> _slices;

        public void Init(Sprite sliceSprite, int price)
        {
            foreach(PizzaSlice slice in _slices)
            {
                slice.Init(sliceSprite, price);
                slice.gameObject.SetActive(true);
            }
        }

        public override ConsumableProduct TakePiece()
        {
          
            if (_slices.Count > 0)
            {
                var pizzaSlice = _slices[_slices.Count - 1];
                _slices.Remove(pizzaSlice);

                return pizzaSlice;
            }

            return null;
        }

        public override void Remove()
        {
            Destroy(gameObject);
        }
    }
}