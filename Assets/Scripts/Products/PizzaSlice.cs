﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class PizzaSlice : ConsumableProduct
    {
        [SerializeField]
        private SpriteRenderer _renderer;

        public override int Price { get { return _price; } }

        private int _price;

        public void Init(Sprite sliceSprite, int price)
        {
            _renderer.sprite = sliceSprite;
            _price = price;
        }

        public override void Consume()
        {
            gameObject.SetActive(false);
            CallConsumeEvent();
        }
    }
}