﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PizzaBar
{
    public class CoinsPanel : MonoBehaviour
    {
        [SerializeField]
        private Text _coinsLabel;

        public void SetCoins(int coins)
        {
            _coinsLabel.text = coins.ToString();
        }
    }
}