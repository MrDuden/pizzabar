﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizzaBar
{
    public class TestTaskScenario : MonoBehaviour
    { 
        public ChefConfig ChefConfig;
        public CustomerConfig CustomerConfig;
        public Chef Chef;
        public CookingTable Table;
        public Customer Customer;
        public ConsumeProductListener ConsumeProductListener;
        public PlayerCoinsObserver PlayerCoinsObserver;
        public PizzaBar PizzaBar;

        public void Start()
        {
            Table.Take(Chef);
            Chef.Init(ChefConfig,Table);
            Chef.StartCooking();
            Customer.Init(CustomerConfig);
            ConsumeProductListener.StartListen();
            PlayerCoinsObserver.StartListen();
            PizzaBar.Init();
            PizzaBar.StartBar();
        }
    }
}